
var mongoose = require('mongoose')
var database = 'api'
/* the server connections string includes both the database server IP address and the name of the database. */
const server = "mongodb://"+ process.env.IP +"/"+database;
console.log(server);
/* the mongoose drivers connect to the server and return a connection object. */
mongoose.connect(server);
const db = mongoose.connection

const listSchema = new mongoose.Schema({
    title: { type: Array, required: true } //Title is an Array.
    
})
/* 'List' collection */
const List = mongoose.model('List', listSchema)
/* END OF MONGOOSE SETUP */

/* notice we are using the 'arrow function' syntax.  */
exports.addList = (data, callback) => {
//create a new 'List' object that adopts the correct schema. */

   
    const title = data[0]; 
    const newList = new List({title})
    newList.save((err, data) => {
      if (err) {
        callback('error: '+err)
      }
      callback(data)
    
  })
  
}

exports.getAll = (callback) => {
  /* the List object contains several useful properties. The 'find' property contains a function to search the MongoDB document collection. */
  List.find( (err, data) => {
    if (err) {
      callback('error: '+err)
    }
    const list = data.map((item) => {
      return {data}
    })
    callback(list)
  })
}


exports.getById = (id, callback) => {
  /* the 'find' property function can take a second 'filter' parameter. */
  List.find({_id: id}, (err, data) => { //This finds the section containing _id
    if (err) {
      callback(err)
    }
    callback(data)
  })
}
exports.clear = (callback) => {
  /* the 'remove()' function removes any document matching the supplied criteria. */
  List.remove({}, (err) =>  {
    if (err) {
      callback('error!')
    }
    callback('lists removed')
  })
}

